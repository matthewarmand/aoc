# Advent of Code

A collection of my [Advent of Code](https://adventofcode.com/) solutions.
Includes source code, input files, and README files with various notes or impressions et cetera.
