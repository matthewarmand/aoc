# Day 5: If You Give A Seed A Fertilizer

<https://adventofcode.com/2023/day/5>

## Part 1

For my first attempt, I tried building out the maps to perform a lookup, which
has turned out horrifically. The actual data contains much higher numbers and
ranges than the sample input, so the script gobbled up all my memory and was
killed.

I've already subclassed `dict` to account for the default lookup behavior, so I
think I can probably create a lookup class that will store these three values
and computationally figure out the value instead of proactively building out the
maps.

### a little while later...

What memory-bombed my machine previously finishes in milliseconds now because
we're just storing what's in the file and then doing arithmetic on lookups.
Very cool, I like what I did. Could maybe do something better with the `locals`
call and local variables, but I think if I made that nicer the lookup would be
way more ugly. This exercise is `AoC: Tradeoffs Edition`.

And it works!

## Part 2

This would be much cleaner if I was on Python 3.12 and could use
[`batched`](https://docs.python.org/3/library/itertools.html#itertools.batched).
I could spin up a docker container for it but I'll just write it for 3.11 instead
since that's my host version on Arch at the moment.

Cool, so similar issue as the first one, the stupid brute-force one doesn't scale
at all. Started running it anyway, and started working on an alternate solution
while it was running. I had trouble wrapping my head around it, so decided to
leave it alone for a day or so which helped for Day 3. I'll come back to that and
try to get a more elegant solution.
