class ComputationalLookupDict(dict):
    def __init__(self):
        super().__init__()
        self.range_rules = []

    def __getitem__(self, lookup):
        # TODO fix this for complicated logic returning multiple intervals
        for key_start, value_start, range_length in self.range_rules:
            if key_start <= lookup < key_start + range_length:
                return lookup + (value_start - key_start)

        return lookup

    def add_range_rule(self, key_start, value_start, range_length):
        self.range_rules.append((key_start, value_start, range_length))
        self.range_rules.sort()


seed_ranges = []
maps = []
with open("inputs/input", "r", encoding="utf-8") as file:
    working_map = None
    for line in file.readlines():
        line = line.strip()
        if not line:
            continue
        if line.startswith("seeds:"):
            seed_ranges = [
                (int(element[0]), int(element[1]))
                for element in line.split(":")[1].strip().split()
            ]
        elif line.endswith("map:"):
            newmap = ComputationalLookupDict()
            maps.append(newmap)
            working_map = newmap
        else:
            value_s, key_s, r_length = [int(val) for val in line.split()]
            if working_map is None:
                raise ValueError("Something's gone wrong")
            working_map.add_range_rule(key_s, value_s, r_length)

current_intervals = seed_ranges
for lookup_map in maps:
    current_intervals = lookup_map[current_intervals]
print(min(val for val, _ in current_intervals))
