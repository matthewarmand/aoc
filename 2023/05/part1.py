class ComputationalLookupDict(dict):
    def __init__(self):
        super().__init__()
        self.range_rules = []

    def __getitem__(self, lookup):
        for key_start, value_start, range_length in self.range_rules:
            if key_start <= lookup < key_start + range_length:
                return lookup + (value_start - key_start)

        return lookup

    def add_range_rule(self, key_start, value_start, range_length):
        self.range_rules.append((key_start, value_start, range_length))
        self.range_rules.sort()


seeds = []
seed_to_soil = ComputationalLookupDict()
soil_to_fertilizer = ComputationalLookupDict()
fertilizer_to_water = ComputationalLookupDict()
water_to_light = ComputationalLookupDict()
light_to_temperature = ComputationalLookupDict()
temperature_to_humidity = ComputationalLookupDict()
humidity_to_location = ComputationalLookupDict()
with open("inputs/input", "r", encoding="utf-8") as file:
    working_map = None
    for line in file.readlines():
        line = line.strip()
        if not line:
            continue
        if line.startswith("seeds:"):
            seeds = [int(seed) for seed in line.split(":")[1].strip().split()]
        elif line.endswith("map:"):
            working_map = locals()[line.split()[0].replace("-", "_")]
        else:
            value_s, key_s, r_length = [int(val) for val in line.split()]
            if working_map is None:
                raise ValueError("Something's gone wrong")
            working_map.add_range_rule(key_s, value_s, r_length)
print(
    min(
        humidity_to_location[
            temperature_to_humidity[
                light_to_temperature[
                    water_to_light[
                        fertilizer_to_water[soil_to_fertilizer[seed_to_soil[seed]]]
                    ]
                ]
            ]
        ]
        for seed in seeds
    )
)
