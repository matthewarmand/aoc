# Day 1: Trebuchet?!

<https://adventofcode.com/2023/day/1>

## Part 1

The first part of Day 1 was pretty straightforward. I used a regex match and
then some python `0`, `-1` array indexing to build the integer and add it to
the running total.

## Part 2

This one was a bit of a pain. My first approach was to iterate over the string
forms of the numbers (`one`, `two`, `three`...) and replace with the digit if
it existed in the test string. That didn't work because I needed to make sure
I was replacing the first one that appeared in the string. So for example, the
input `twone` would become `tw1` because I was checking for `one` first, but
`two` should have been caught. I used an iteration through the string itself
to check for the numbers character-by-character.

That still didn't work, and I was passing the example case. Something similar
to the above issue was still happening... the example `eightwothree` should
morph to `823`, and not `8wo3` as mine was doing. I eschewed morphing the
input strings along the way and just built new strings that kept track of
the digits. It would pass through digits from the orginal string, as well as
digits for the word-forms. All good after making that fix.
