import re

with open("inputs/input", "r", encoding="utf-8") as file:
    total = 0
    for line in file.readlines():
        index = 0
        new_line = ""
        while True:
            if not line[index].isalpha():
                new_line += line[index]
            else:
                substring = line[index:]
                for string, digit in (
                    ("one", "1"),
                    ("two", "2"),
                    ("three", "3"),
                    ("four", "4"),
                    ("five", "5"),
                    ("six", "6"),
                    ("seven", "7"),
                    ("eight", "8"),
                    ("nine", "9"),
                ):
                    if substring.startswith(string):
                        new_line += digit
                        break

            index += 1
            if index >= len(line):
                break
        digits = re.findall(r"\d", new_line)
        total += int("".join([digits[0], digits[-1]]))
print(total)
