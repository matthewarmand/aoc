import re

with open("inputs/input", "r", encoding="utf-8") as file:
    total = 0
    for line in file.readlines():
        digits = re.findall(r"\d", line)
        total += int("".join([digits[0], digits[-1]]))
print(total)
