max_map = {
    "red": 12,
    "green": 13,
    "blue": 14,
}
total = 0
with open("inputs/input", "r", encoding="utf-8") as file:
    for line in file.readlines():
        game_name, pulls = [piece.strip() for piece in line.split(":")]
        game_id = int(game_name.split(" ")[1].strip())
        invalid = False
        for pull in pulls.split(";"):
            for entry in pull.strip().split(","):
                number, color = entry.strip().split(" ")
                if int(number) > max_map[color]:
                    invalid = True
                    break
            if invalid:
                break
        if not invalid:
            total += game_id
print(total)
