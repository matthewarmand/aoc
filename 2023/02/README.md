# Day 2: Cube Conundrum

<https://adventofcode.com/2023/day/2>

## Part 1

Pretty straightforward, just a bunch of `split`s, checking the values for each
color against the maximum, discarding invalid games and adding valid game IDs
to a running total. Only issue I ran into was a type error because I forgot to
`int`-cast the number after `split`ing.

## Part 2

If anything, slightly logically easier than part 1? Just need to keep track of
the maximum number pulled for each color (`dict`) and then iterate over those
values to build the power and add to running total.
