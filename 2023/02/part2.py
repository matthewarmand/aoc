total = 0
with open("inputs/input", "r", encoding="utf-8") as file:
    for line in file.readlines():
        game_name, pulls = [piece.strip() for piece in line.split(":")]
        game_id = int(game_name.split(" ")[1].strip())
        min_map = {}
        for pull in pulls.split(";"):
            for entry in pull.strip().split(","):
                number, color = entry.strip().split(" ")
                number = int(number)
                if color not in min_map or number > min_map[color]:
                    min_map[color] = number
        power = 1
        for value in min_map.values():
            power *= value
        total += power
print(total)
