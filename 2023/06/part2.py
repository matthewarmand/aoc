race = []
with open("inputs/input", "r", encoding="utf-8") as file:
    for line in file.readlines():
        race.append(int("".join(line.split(":")[1].split())))
total = 0
for hold_time in range(1, race[0]):
    if hold_time * (race[0] - hold_time) > race[1]:
        total += 1
print(total)
