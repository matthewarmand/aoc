# Day 6: Wait For It

<https://adventofcode.com/2023/day/6>

## Part 1
Pleasantly much easier than Day 5.

## Part 2
Interestingly much shorter and easier code (still pretty equally simple) than
part 1.
