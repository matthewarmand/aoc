from functools import reduce

races = []
with open("inputs/input", "r", encoding="utf-8") as file:
    for line in file.readlines():
        field, num_str = line.split(":")
        nums = (int(num.strip()) for num in num_str.strip().split())
        if field == "Time":
            races.extend([[num] for num in nums])
        elif field == "Distance":
            for i, num in enumerate(nums):
                races[i].append(num)
ways_to_win = []
for duration, record_distance in races:
    winners = []
    for hold_time in range(1, duration):
        if hold_time * (duration - hold_time) > record_distance:
            winners.append(hold_time)
    ways_to_win.append(len(winners))
print(reduce(lambda x, y: x * y, ways_to_win))
