def smart_split_line(source_line):
    tracker = ""
    pieces = []
    for char in source_line.strip():
        if char == ".":
            if tracker:
                pieces.append(tracker)
                tracker = ""
            pieces.append("")
        elif not char.isdigit():
            if tracker:
                pieces.append(tracker)
                tracker = ""
            pieces.append(char)
        else:
            tracker += char
    if tracker:
        pieces.append(tracker)
    return pieces


with open("inputs/input", "r", encoding="utf-8") as file:
    symbol_coordinates = []
    number_coordinates = {}
    row_index = 0
    for line in file.readlines():
        column_index = 0
        for piece in smart_split_line(line):
            if piece:
                if piece.isdigit():
                    if piece not in number_coordinates:
                        number_coordinates[piece] = []
                    for col in range(len(piece)):
                        number_coordinates[piece].append(
                            (row_index, column_index + col)
                        )
                else:
                    symbol_coordinates.append((row_index, column_index))
                column_index += len(piece)
            else:
                column_index += 1
        row_index += 1

valid_pieces = []
for symbol_row, symbol_col in symbol_coordinates:
    for number, coordinates in number_coordinates.items():
        found_for_this_number = False
        for number_row, number_col in coordinates:
            if abs(symbol_row - number_row) <= 1 and abs(symbol_col - number_col) <= 1:
                valid_pieces.append(int(number))
                found_for_this_number = True
                break
        if found_for_this_number:
            continue
print(sum(valid_pieces))
