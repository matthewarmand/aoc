# Day 3: Gear Ratios

<https://adventofcode.com/2023/day/3>

## Part 1

Annoyingly finnicky, I'm still stuck on this one so far... I have some code that
should work in theory, and does work on the sample set, but isn't working on the
final input so far.

I got a bit caught up in trying to do this in one iteration of the file, and I
think that's caused some complexity that I didn't need. Though it's less efficient
and less elegant (for a given definition of elegant), I think I'll refactor it
allowing myself a couple iterations of the file to build up the data so I can
solve it in a more readable way.

### Attempt 2 update

Started rewriting this and then went to check something in the source data. I
was basically uniquifying the part numbers I was adding in, and that was incorrect
because some parts show up in multiple places. So I fixed that and huzzah, it
all works. Funny what a 24 hour break will do in approaching the problem fresh.

## Part 2

My original data structures were very nearly perfect for this already, except
that instead of just keeping track of the symbol coordinates I need to track
which symbols are at which coordinates. That was a very easy change with a
slight extension of the tuple, then re-used almost all of my existing logic to
track the asterisks that had exactly 2 adjacent parts, and keep a running total.
