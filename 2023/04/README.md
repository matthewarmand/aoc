# Day 4: Scratchcards

<https://adventofcode.com/2023/day/4>

## Part 1

Some easy split and set arithmetic, pleasingly easy after Day 3 has given me so
much trouble.

## Part 2

Still pretty straightforward, got tripped up a little bit by forgetting to
account for the bonus cards in counting up subsequent winners, but still fairly
simple overall.
