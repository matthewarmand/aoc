total = 0
with open("inputs/input", "r", encoding="utf-8") as file:
    for line in file.readlines():
        winning_numbers, card_numbers = [
            nums.strip().split() for nums in line.split(":")[1].strip().split("|")
        ]
        if winning_set := {num for num in card_numbers if num in winning_numbers}:
            total += 2 ** (len(winning_set) - 1)
print(total)
