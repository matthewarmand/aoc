from collections import defaultdict

with open("inputs/input", "r", encoding="utf-8") as file:
    card_counts = defaultdict(lambda: 0)
    card_ids = []
    for line in file.readlines():
        card_title, number_lists = line.split(":")
        card_id = card_title.split()[1]
        card_ids.append(card_id)
        winning_numbers, card_numbers = [
            nums.strip().split() for nums in number_lists.strip().split("|")
        ]
        number_of_these_cards = 1 + card_counts[card_id]
        for winner in range(
            int(card_id) + 1,
            int(card_id)
            + len({num for num in card_numbers if num in winning_numbers})
            + 1,
        ):
            card_counts[str(winner)] += number_of_these_cards
print(
    len(card_ids)
    + sum(value for (key, value) in card_counts.items() if key in card_ids)
)
