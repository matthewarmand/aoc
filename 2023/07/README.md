# Day 7: Camel Cards

<https://adventofcode.com/2023/day/7>

## Part 1

Soo, I was a bit foolish here and didn't read the directions clearly enough. I
was using the wrong comparison logic because I skipped over the section where it
tells you to compare against the first card if they're the same type (full house,
4 of a kind, et cetera). Spent a looong time debugging that before I finally
tracked back and figured that out.

Once that was figured out, and I modified my logic to account for that, all my
sorting worked great.

## Part 2

Actually flew through this one pretty quickly by comparison, partially because I
read it much more carefully. Just took a shift in my precedence alphabet, and
then an extra block in my grouping logic to apply J's to the most beneficial
other card.
