from itertools import groupby
from collections import Counter

hands_with_bets = []
precedence = "AKQJT98765432"
with open("inputs/input", "r", encoding="utf-8") as file:
    for line in file.readlines():
        hand, bet_str = [e.strip() for e in line.split()]
        hands_with_bets.append((hand, int(bet_str)))


def card_precedence_key(cards):
    divisor = 100
    ret = 0.0
    for card in cards:
        ret += precedence.index(card) / divisor
        divisor *= 100
    return ret


def ary_eq(ary1, ary2):
    return Counter(ary1) == Counter(ary2)


def sort_hands(hand_with_bet):
    groups = sorted(
        [
            (card, len(list(group)))
            for card, group in groupby(sorted(hand_with_bet[0], key=precedence.index))
        ],
        key=lambda c: -(c[1]),
    )
    group_counts = [g[1] for g in groups]
    base_key = None
    if len(groups) == 1:  # 5 of a kind
        base_key = 0
    elif len(groups) == 2:
        if ary_eq([4, 1], group_counts):  # 4 of a kind
            base_key = 1
        elif ary_eq([3, 2], group_counts):  # full house
            base_key = 2
    elif len(groups) == 3:
        if ary_eq([3, 1, 1], group_counts):  # 3 of a kind and 2 high cards
            base_key = 3
        elif ary_eq([2, 2, 1], group_counts):  # 2 pair
            base_key = 4
    elif len(groups) == 4:  # one pair
        base_key = 5
    elif len(groups) == 5:  # high cards
        base_key = 6
    return base_key + card_precedence_key(hand_with_bet[0])


hands_with_bets.sort(key=sort_hands)
total_hands = len(hands_with_bets)
print(
    sum(
        hand_with_bet[1] * (total_hands - index)
        for index, hand_with_bet in enumerate(hands_with_bets)
    )
)
