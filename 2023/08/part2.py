import re
from math import lcm

network_map = {}
instructions = []
with open("inputs/input", "r", encoding="utf-8") as file:
    for index, line in enumerate(file.readlines()):
        if index == 0:
            for i in line:
                if i == "L":
                    instructions.append(0)
                elif i == "R":
                    instructions.append(1)
        elif line.strip():
            pieces = re.findall(r"[1-9A-Z]+", line)
            network_map[pieces[0]] = pieces[1:]
lookup_keys = [k for k in network_map if k.endswith("A")]
instruction_count = len(instructions)
solutions = []
for key in lookup_keys:
    instruction_index = 0
    while True:
        key = network_map[key][instructions[instruction_index % instruction_count]]
        instruction_index += 1
        if key.endswith("Z"):
            solutions.append(instruction_index)
            break
print(lcm(*solutions))
