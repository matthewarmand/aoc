# Day 8: Haunted Wasteland

<https://adventofcode.com/2023/day/8>

## Part 1

I was up late last night finishing Day 7, so glanced at the problem when it was
posted. Part 1 seemed pretty simple, just building the map and an instruction
array, and then looping through that until the right node was found. It proved
to be the case, now on to part 2 which I'm sure will be much harder.

## Part 2

This could get very prickly with the size of the network map. I've modified my
code in such a way that it works for the example, but it's been running for
quite some time now.

This is probably one of those problems that doesn't scale well and has some
magical solution that takes like 2 seconds somehow... I'll keep poking at it
while it's running, but I'm unsure what sorcery I'll be able to work.

### some time later...

Yep, I started thinking about this from a mathematical perspective and thought
it might be either the Least Common Multiple or the product of each of the
individual solutions. So instead of actually traversing them all simultaneously,
I computed them each individually (much faster) and then took the LCM. After
fixing a bug where I'd accidentally left the counter variable 1 level too high
in looping scope, I got the correct answer.

I've seen some people complaining about this problem and yeah to be honest I
didn't really love it either at first, but I arrived at the "right" solution
much more quickly/intuitively/independently than the Day 5 part 2 abomination.
So actually I don't mind this one (results-oriented thinking and all that).
