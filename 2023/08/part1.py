import re

network_map = {}
instructions = []
with open("inputs/input", "r", encoding="utf-8") as file:
    for index, line in enumerate(file.readlines()):
        if index == 0:
            for i in line:
                if i == "L":
                    instructions.append(0)
                elif i == "R":
                    instructions.append(1)
        elif line.strip():
            pieces = re.findall(r"[A-Z]+", line)
            network_map[pieces[0]] = pieces[1:]
lookup_key = "AAA"
target = "ZZZ"
instruction_index = 0
instruction_count = len(instructions)
while True:
    lookup_key = network_map[lookup_key][
        instructions[instruction_index % instruction_count]
    ]
    instruction_index += 1
    if lookup_key == target:
        print(instruction_index)
        break
